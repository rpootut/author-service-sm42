<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class AuthorController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * List the authors.
     *
     * @return void
     */
    public function index()
    {
        $authors = Author::all();
        return $this->successResponse($author);
    }

     /**
     * Store an author.
     *
     * @return void
     */
    public function store(Request $request)
    {
       $rules = [
           'name'=> 'required|max:100',
           'gender'=> 'required|in:male,female',
           'country'=> 'required|max:80'
       ];

       $this->validate($request, $rules);

       $author = Author::create($request->all());

       return $this->successResponse($author, Response::HTTP_CREATED);
    }

     /**
     * Show an author.
     *
     * @return void
     */
    public function show($author)
    {
        $author = Author::findOrFail($author);
        
        return $this->successResponse($author);        
    }

     /**
     * Update an author.
     *
     * @return void
     */
    public function update(Request $request, $author)
    {
        
    }

    /**
     * Destroy an author.
     *
     * @return void
     */
    public function destroy($author)
    {
        
    }
}
